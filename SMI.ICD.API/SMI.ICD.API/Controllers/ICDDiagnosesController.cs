﻿using Entities.Models.RestRequest;
using Microsoft.AspNetCore.Mvc;
using ReaderServices.Service;
using System;
using System.Threading.Tasks;
using WriterServices.Service;

namespace SMI.ICD.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ICDDiagnosesController : Controller
    {
        #region Private Members

        /// <summary>
        /// A private instance member from <see cref="IReaderService"/> 
        /// </summary>
        private readonly IReaderService reader;

        /// <summary>
        /// A private instance member from <see cref="IWriterService"/> 
        /// </summary>
        private readonly IWriterService writer;


        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="reader">A single instance paramater <see cref="IReaderService"/></param>
        /// <param name="reader">A single instance paramater <see cref="IWriterService"/></param>
        public ICDDiagnosesController(IReaderService reader, IWriterService writer)
        {
            this.writer = writer;
            this.reader = reader;
        }

        #endregion

        [HttpGet, Route("Get")]
        public async Task<IActionResult> GetICDDiagnoses(int page = 0, int itemPerpage = 10, string clientKey = null, string secretKey = null)
        {
            var result = await reader.FetchICDAsync();

            if (result != null)
            {
                return Ok(result);
            }

            throw new ArgumentNullException($"Opss.. something wrong! or item is empty");

        }

        [HttpGet, Route("Get/{id}")]
        public async Task<IActionResult> GetICDDiagnoses(int ? id, string clientKey = null, string secretKey = null)
        {
            var result = await reader.FetchICDAsync(id.Value);

            if (result != null)
            {
                return Ok(result);
            }

            throw new ArgumentNullException($"Opss.. something wrong! or item is empty");

        }

        [HttpPost, Route("Search")]
        public async Task<IActionResult> SearchICDDiagnoses(SearchDiagnoses searchDiagnoses)
        {
            var result = await reader.FetchICDDiagnosesAsync(searchDiagnoses);

            if(result != null)
            {
                return Ok(result);
            }

            throw new ArgumentNullException($"Opss.. something wrong! or item is empty");

        }

       
    }
}