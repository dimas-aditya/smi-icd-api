﻿using System;

namespace Entities.Models
{
    public class ICD
    {
        public int ? ID { get; set; }
        public string ICDChapter { get; set; }
        public string ICDCode { get; set; }
        public string ICDBlock { get; set; }
        public string ICDDescription { get; set; }
        public string PreDiagnoseCode1 { get; set; }
        public string PreDiagnose1 { get; set; }
        public string IcdCode1 { get; set; }
        public string IcdCode2{ get; set; }
        public string IcdCode3{ get; set; }
        public string IcdCode4{ get; set; }
        public string IcdCode5 { get; set; }
        public string Diagnose1 { get; set; }
        public string Diagnose2 { get; set; }
        public string Diagnose3 { get; set; }
        public string Diagnose4 { get; set; }
        public string Diagnose5 { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
