﻿using Dapper;
using Entities.Models;
using Entities.Models.RestRequest;
using Microsoft.Extensions.Options;
using Microsoft.VisualBasic;
using ReaderServices.Config;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ReaderServices.Service
{
    public class ReaderService : IReaderService
    {

        #region Private Helpers

        /// <summary>
        /// A single instance member from <see cref="ReaderConfig"/> 
        /// </summary>
        private readonly IOptions<ReaderConfig> options;

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="options">A single instance parameter from <see cref="ReaderConfig"/></param>
        public ReaderService(IOptions<ReaderConfig> options) => this.options = options;

        public async Task<IEnumerable<ICD>> FetchICDAsync(string clientKey = null, string secretkey = null)
        {
            try
            {
                using (var connection = new SqlConnection(options.Value.DatabaseConnection))
                {
                    connection.Open();

                    options.Value.DatabaseQuery = @"SELECT [id] AS ID
                                                        ,[icd_chapter] AS ICDChapter
                                                        ,[icd_block] AS ICDBlock
                                                        ,[icd_code] AS ICDCode
                                                        ,[icd_description] AS ICDDescription
                                                        ,[create_by] AS CreateBy
                                                        ,[create_date] AS CreateDate
                                                        ,[update_by] AS UpdateBy
                                                        ,[update_date] AS UpdateDate
                                                    FROM [dbo].[icd] WHERE isDeleted = 0";

                    // Fetch data for one table
                    var providers = await connection.QueryAsync<ICD>(
                            sql: options.Value.DatabaseQuery,
                            commandType: System.Data.CommandType.Text
                        );

                    return providers;

                }
            }

            catch (Exception ex)
            {
                // A debugger exception information for developer.
                Debug.WriteLine($"An error occurred: {ex.Message}, Trace: {ex.StackTrace}");

                // Throw an exception.
                throw new ArgumentException($"An error occurred: {ex.Message}, Trace: {ex.StackTrace}");
            }
        }

        public async Task<IEnumerable<ICD>> FetchICDAsync(int id, string clientKey = null, string secretkey = null)
        {
            try
            {
                using (var connection = new SqlConnection(options.Value.DatabaseConnection))
                {
                    connection.Open();

                    options.Value.DatabaseQuery = @"
                                     select  c.id,
                    c.initial_diagnose_icd_id,
                    (select icd_code  from  icd i where i.id = c.initial_diagnose_icd_id  ) PreDiagnoseCode1,
                    (select icd_description  from  icd i where i.id = c.initial_diagnose_icd_id  ) PreDiagnose1,
					c.final_diagnose_icd_1_id,
                    (select icd_code  from  icd i where i.id = c.final_diagnose_icd_1_id  ) IcdCode1,
                    (select icd_description  from  icd i where i.id = c.final_diagnose_icd_1_id  ) Diagnose1,
                    c.final_diagnose_icd_2_id,
                    (select icd_code  from  icd i where i.id = c.final_diagnose_icd_2_id  ) IcdCode2,
                    (select icd_description  from  icd i where i.id = c.final_diagnose_icd_2_id  ) Diagnose2,
                    c.final_diagnose_icd_3_id,
                    (select icd_code  from  icd i where i.id = c.final_diagnose_icd_3_id  ) IcdCode3,
                    (select icd_description  from  icd i where i.id = c.final_diagnose_icd_3_id  ) Diagnose3,
                    c.final_diagnose_icd_4_id,
                    (select icd_code  from  icd i where i.id = c.final_diagnose_icd_4_id  ) IcdCode4,
                    (select icd_description  from  icd i where i.id = c.final_diagnose_icd_4_id  ) Diagnose4,
                    c.final_diagnose_icd_5_id,
                    (select icd_code  from  icd i where i.id = c.final_diagnose_icd_5_id  ) IcdCode5,
                    (select icd_description  from  icd i where i.id = c.final_diagnose_icd_5_id  ) Diagnose5
                    from cases c where c.id= @ID AND isDeleted = 0";  

                    // Fetch data for one table
                    var providers = await connection.QueryAsync<ICD>(
                            sql: options.Value.DatabaseQuery,
                            param: new
                            {
                                @ID = id
                            },
                            commandType: System.Data.CommandType.Text
                        );

                    return providers;

                }
            }

            catch (Exception ex)
            {
                // A debugger exception information for developer.
                Debug.WriteLine($"An error occurred: {ex.Message}, Trace: {ex.StackTrace}");

                // Throw an exception.
                throw new ArgumentException($"An error occurred: {ex.Message}, Trace: {ex.StackTrace}");
            }
        }

        public async Task<IEnumerable<ICD>> FetchICDDiagnosesAsync(SearchDiagnoses searchDiagnoses)
       

        {
            try
            {
                using (var connection = new SqlConnection(options.Value.DatabaseConnection))
                {
                    connection.Open();

                    //var ICDS = await connection.QueryAsync<ICD>(
                    //          sql: options.Value.ICDQuery.Search,
                    //          param: new
                    //          {
                    //         //     @ID = searchDiagnoses.ID,
                    //              @ICDCode = searchDiagnoses.ICDCode,
                    //          //    @ICDChapter = searchDiagnoses.ICDChapter,
                    //           //   @ICDBlock = searchDiagnoses.ICDBlock,
                    //              @ICDDescription = searchDiagnoses.ICDDescription,

                    //          },
                    //          commandType: System.Data.CommandType.Text
                    //      );






                    // icd_chapter = (CASE WHEN " + searchDiagnoses.ICDChapter + " > 0 THEN " + searchDiagnoses.ICDChapter + "   ELSE icd_chapter END)  " + " and
                    //icd_block = (CASE WHEN " + searchDiagnoses.icd_block + " > 0 THEN " + searchDiagnoses.ICDChapter + "   ELSE icd_chapter END)  " + "  and

                    options.Value.DatabaseQuery = @"SELECT [id] AS ID         ,[icd_chapter] AS ICDChapter   
,[icd_block] AS ICDBlock    ,[icd_code] AS ICDCode  
,[icd_description] AS ICDDescription   ,[create_by] AS CreateBy      ,[create_date] AS CreateDate
,[update_by] AS UpdateBy         ,[update_date] AS UpdateDate    FROM [dbo].[icd]                                              
WHERE   icd_code LIKE '" + searchDiagnoses.ICDCode + "%' and  icd_description LIKE '%" + searchDiagnoses.ICDDescription + "%' ";




                    //   [icd_chapter] like  '" + searchDiagnoses.ICDChapter + "%'
                    //AND[icd_block] like   '" + searchDiagnoses   + "%'  " +
                    //AND[icd_code] like   '" +  @ICDCode + "%'    " +
                    //AND[icd_description] like   '" + %@ICDDescription + "%'

                  
                    var ICDS = await connection.QueryAsync<ICD>(
                            sql: options.Value.DatabaseQuery,
                            param: new
                            {

                               
                                @ICDBlock = searchDiagnoses.ICDBlock,
                                @ICDChapter = searchDiagnoses.ICDChapter,
                                @ICDCode = searchDiagnoses.ICDCode,
                                @ICDDescription = searchDiagnoses.ICDDescription,
                                @ID = searchDiagnoses.ID
                            },
                            commandType: System.Data.CommandType.Text
                        );

                    return ICDS;

                }
            }

            catch (Exception ex)
            {
                // A debugger exception information for developer.
                Debug.WriteLine($"An error occurred: {ex.Message}, Trace: {ex.StackTrace}");

                // Throw an exception.
                throw new ArgumentException($"An error occurred: {ex.Message}, Trace: {ex.StackTrace}");
            }
        }
         
        #endregion

    }
}
