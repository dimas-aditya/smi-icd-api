﻿using System;

namespace Entities.Models
{
    public class Cases
    {
        public int? ID { get; set; }
        public int? MedicalServiceID { get; set; } = 0;
        public int? SubMedicalServiceID { get; set; } = 0;
        public int? PlanID { get; set; } = 0;
        public string PlanCode { get; set; }
        public int? ProviderID { get; set; } = 0;
        public int? BeneficiaryID { get; set; }
        public int? ClaimTypeID { get; set; }
        public int? CaseStatusID { get; set; }
        public string DoctorName { get; set; }
        public string CaseNo { get; set; }
        public string CaseType { get; set; }
        public string RegisterDate { get; set; }
        public string AdmissionDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
        public string DischargeDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
        public int? InitialDiagnoseICDID { get; set; }
        public int? InitialDiagnoseICD2ID { get; set; }
        public int? InitialDiagnoseICD3ID { get; set; }
        public int? InitialDiagnoseICD4ID { get; set; }
        public int? InitialDiagnoseICD5ID { get; set; }
        public string InitialDiagnoseDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
        public string InitialDiagnoseCause { get; set; }
        public int? FinalDiagnoseICD1ID { get; set; }
        public int? FinalDiagnoseICD2ID { get; set; }
        public int? FinalDiagnoseICD3ID { get; set; }
        public int? FinalDiagnoseICD4ID { get; set; }
        public int? FinalDiagnoseICD5ID { get; set; }
        public string FinalDiagnoseCause { get; set; }
        public string HospitalizeIndiciation { get; set; }
        public string RemarkItemID { get; set; }
        public int? RequestorTypeID { get; set; }
        public string PathBeneficiarySignature { get; set; }
        public string PathBeneficiaryPhoto { get; set; }
        public string PathBeneficiaryIDCard { get; set; }
        public int? CaseID { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; } = DateTime.Now;
        public int? BeneficiaryRelationID { get; set; }
        public bool IsDeleted { get; set; } = false;
        public string PreDiagnosis { get; set; }
        public string RoomType { get; set; }
        public decimal RoomPrice { get; set; }
    }
}
