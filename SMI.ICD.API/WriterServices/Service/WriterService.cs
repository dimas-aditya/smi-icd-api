﻿using Microsoft.Extensions.Options;
using WriterServices.Config;

namespace WriterServices.Service
{
    public class WriterService : IWriterService
    {
        #region Private Members

        /// <summary>
        /// A single instance member from <see cref="WriterConfig"/> 
        /// </summary>
        private readonly IOptions<WriterConfig> options;

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="options">A single instance parameter from <see cref="WriterConfig"/></param>
        public WriterService(IOptions<WriterConfig> options) 
        {
            this.options = options;
        }

        #endregion


  

    }
}
