﻿namespace Entities.Models.RestRequest
{
    public class SearchDiagnoses
    {
         public int ? ID { get; set; }
        public string ICDCode { get; set; }
        public string ICDChapter { get; set; }
        public string ICDBlock { get; set; }
        public string ICDDescription { get; set; }
    }
}
