﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models
{
   public class ICDQuery
    {
        public string Search { get; set; }
        public string Get { get; set; }
        public string GetParams { get; set; }
    }
}
