﻿using Dapper;
using Entities.Models;
using RestSharp;

namespace ReaderServices.Config
{
    public class ReaderConfig
    {
        public string ClientURL { get; set; }
        public string RequestURL { get; set; }
        public string DatabaseConnection { get; set; }
        public string DatabaseQuery { get; set; }

        public ICDQuery ICDQuery { get; set; }
        public DynamicParameters DatabaseQueryParams { get; set; }
    }
}
