using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ReaderServices.Config;
using ReaderServices.Service;
using WriterServices.Config;
using WriterServices.Service;

namespace SMI.ICD.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.Configure<ReaderConfig>(options =>
            {
                options.DatabaseConnection = Configuration.GetValue<string>("DatabaseConnection");
            });

            services.Configure<WriterConfig>(options =>
            {
                options.DatabaseConnection = Configuration.GetValue<string>("DatabaseConnection");
            });

            services.AddSingleton<IReaderService, ReaderService>();
            services.AddSingleton<IWriterService, WriterService>();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "SMI ICD API",
                    Description = "SMI API",
                    TermsOfService = new Uri(@"https://www.rds.co.id/"),
                    Contact = new OpenApiContact
                    {
                        Name = "Syntechmi.com",
                        Email = "support@syntechmi.com",
                        Url = new Uri(@"https://www.rds.co.id/contact-us"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "SMI License",
                        Url = new Uri(@"https://www.rds.co.id/"),
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SMI ICD API");
                c.RoutePrefix = string.Empty;
            });


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
