﻿using Dapper;

namespace WriterServices.Config
{
    public class WriterConfig
    {
        public string DatabaseConnection { get; set; }
        public string DatabaseQuery { get; set; }
        public DynamicParameters DatabaseQueryParams { get; set; }


    }
}
