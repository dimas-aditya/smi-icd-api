﻿using Entities.Models;
using Entities.Models.RestRequest;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReaderServices.Service
{
    public interface IReaderService
    {
        Task<IEnumerable<ICD>> FetchICDAsync(string clientKey = null, string secretkey = null);
        Task<IEnumerable<ICD>> FetchICDAsync(int id, string clientKey = null, string secretkey = null);
       
        Task<IEnumerable<ICD>> FetchICDDiagnosesAsync(SearchDiagnoses searchDiagnoses);

    }
}
